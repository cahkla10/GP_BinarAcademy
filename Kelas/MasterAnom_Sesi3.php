<?php
//TUGAS OOP BASIC
	class siswa{
		public $nama;
		public $nis;
		public $nisn;
		public $kelas;
		
		// set property
		
		public function setNama($inputNama){
			$this->nama=$inputNama;
		}
		
		public function setNis($inputNis){
			$this->nis=$inputNis;
		}
		
		public function setNisn($inputNisn){
			$this->nisn=$inputNisn;
		}
		
		public function setKelas($inputKelas){
			$this->kelas=$inputKelas;
		}
		
		//get property
		
		public function getNama(){
			return $this->nama;
		}
		
		public function getNis(){
			return $this->nis;
		}
		
		public function getNisn(){
			return $this->nisn;
		}
		
		public function getKelas(){
			return $this->kelas;
		}
	}
	
	//object
	
	$agus=new siswa;
	$budi=new siswa;
	
    $agus->setNama("Agus");
    $agus->setNis(1234);
    $agus->setNisn(1234567);
    $agus->setKelas("X IPA 1");
    
    $budi->nama="Budi";
    $budi->nis=5678;
    $budi->nisn=9865214;
    $budi->kelas="X IPA 2";
    
    
    
    echo "Nama: ".$agus->getNama()."<br>";
    echo "NIS: ".$agus->getNis()."<br>";
    echo "NISN: ".$agus->getNisn()."<br>";
    echo "Kelas: ".$agus->getKelas()."<br><br>";
    
    echo "Nama: ".$budi->nama."<br>";
    echo "NIS: ".$budi->nis."<br>";
    echo "NISN: ".$budi->nisn."<br>";
    echo "Kelas: ".$budi->kelas;
?>